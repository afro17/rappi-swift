//
//  AccountRepository.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import Alamofire

class Accountrepository {
    static func registerUserOnServer(params: [String: Any], completion: @escaping((Bool) -> Void)) {
        let url = Constants.Api.URL + Constants.endpoints.users
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: Base.shared.userHeaders)
            .responseJSON {  response in
                if response.result.isSuccess {
                    if response.response?.statusCode == 201 {
                        completion(true)
                    } else {
                        completion(false)
                    }
                } else {
                    completion(false)
                }
        }
    }
}
