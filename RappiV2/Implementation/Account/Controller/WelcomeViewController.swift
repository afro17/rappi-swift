//
//  WelcomeViewController.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import GoogleSignIn

class WelcomeViewController: UIViewController {
    
    //MARK: - Properties
    private let presenter = LoginPresenter()
    
    //MARK: - IBOutlets
    @IBOutlet weak var fbLoginButton: FBSDKLoginButton!
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.presenter.attachView(self)
        
        fbLoginButton.delegate = self
        fbLoginButton.readPermissions = ["public_profile", "email"]
        //Google SignIn
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    static func storyboardInstance() -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.Storyboards.Account, bundle:nil)
        return storyBoard.instantiateViewController(withIdentifier: Constants.ViewControllers.WelcomeNC)
    }
}

//MARK: - Facebook delegate
extension WelcomeViewController: FBSDKLoginButtonDelegate {
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if let error = error {
            print(error.localizedDescription)
        } else {
            if let token = FBSDKAccessToken.current().tokenString {
                let credential = FacebookAuthProvider.credential(withAccessToken: token)
                self.showProgress("Por favor espere")
                self.presenter.saveUser(credential: credential)
            }
            
        }
    }
}

//MARK: - Google SigIn delegate
extension WelcomeViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        if error != nil {
            self.hideProgress() { _ in
                self.alertMessage(title: "Iniciar Sesión", message: "Se presentó un error iniciando sesión")
            }
        } else {
            self.showProgress("Por favor espere")
            guard let authentication = user.authentication else { return }
            let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                           accessToken: authentication.accessToken)
            self.presenter.saveUser(credential: credential)
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print(error.localizedDescription)
        self.hideProgress() { _ in
            self.alertMessage(title: "Iniciar Sesión", message: "Se presentó un error iniciando sesión")
        }
    }
}

extension WelcomeViewController: LoginView {
    func didLoginSuccess() {
        self.hideProgress() { _ in
            var destination: UIViewController!
            if !PreferencesManager.getBool(key: Constants.Preferences.canPlay) {
                destination = IntroViewController.storyboardInstance()
            } else {
                destination = MainViewController.storyboardInstance()
            }
            self.present(destination, animated: true)
        }
    }
}
