//
//  LoginPresenter.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import Firebase

protocol LoginView {
    func didLoginSuccess()
}

class LoginPresenter {
    private var view: LoginView?
    
    func attachView(_ view: LoginView) {
        self.view = view
    }
    
    func saveUser(credential: AuthCredential) {
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            } else {
                if let user = Auth.auth().currentUser {
                    let params: [String: String]  = [
                        "name": user.displayName ?? "no name",
                        "uuid": user.uid
                    ]
                    Accountrepository.registerUserOnServer(params: params) { success in
                        print("user created: \(success)")
                    }
                }
                PreferencesManager.setBool(key: Constants.Preferences.IsLoggedInUser, value: true)
                self.view?.didLoginSuccess()
            }
        }
    }
}
