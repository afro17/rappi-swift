//
//  ProductsPresenter.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/20/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation

protocol CorridorsView {
    func didShowCorridors()
}

class CorridorsPresenter {
    //MARK: - Properties
    private var view: CorridorsView?
    var corridors: [Corridor]?
    
    func attachView(_ view: CorridorsView) {
        self.view = view
    }
    
    func getCorridors() {
        CorridorsRepository.getCorridorsFromServer() { corridors in
            self.corridors = corridors?.filter({$0.products?.count ?? 0 > 0})
            DispatchQueue.main.async {
                self.view?.didShowCorridors()
            }
        }
    }
}
