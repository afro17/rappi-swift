//
//  ProductTableViewCell.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/20/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import UIKit

class CorridorTableViewCell: UITableViewCell {
    
    //MARK: - Properties
    var corridor: Corridor! {
        didSet {
            self.showInfo()
        }
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var corridorNameLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.cardView.layer.cornerRadius = 10
        self.cardView.clipsToBounds = true
        self.cardView.layer.masksToBounds = true
    }
    
    private func showInfo() {
        self.collectionView.reloadData()
        self.corridorNameLabel.text = self.corridor.name
    }
}

extension CorridorTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return corridor.products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.Cells.Product, for: indexPath) as! ProductCollectionViewCell
        let product = self.corridor.products![indexPath.row]
        cell.product = product
        
        return cell
    }
}
