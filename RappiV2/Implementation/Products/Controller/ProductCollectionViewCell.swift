//
//  ProductCollectionViewCell.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/20/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCollectionViewCell: UICollectionViewCell {
    //MARK: - Properties
    var product: Product! {
        didSet {
            self.showInfo()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 7
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = UIImage(named: "no_image_2")
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    //MARK: - Functions
    private func showInfo() {
        if let url = URL(string: product.image ?? "") {
            SDWebImageManager.shared().imageDownloader?.downloadImage(with: url, options: .continueInBackground, progress: nil) { (image, error, type, url) in
                if image != nil {
                    DispatchQueue.main.async {
                        self.imageView.image = image
                    }
                } else {
                    DispatchQueue.main.async {
                    self.imageView.image = UIImage(named: "no_image_2")
                    }
                }
            }
        }
        self.nameLabel.text = product.name
        self.priceLabel.text = "$\(product.price ?? 0)"
    }
}
