//
//  ProductsViewController.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/20/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import UIKit
import UIEmptyState

class CorridorsViewController: UIViewController {
    //MARK: - Properties
    private let presenter = CorridorsPresenter()

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter.attachView(self)
        
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    static func storyboardInstance() -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.Storyboards.Products, bundle:nil)
        return storyBoard.instantiateViewController(withIdentifier: Constants.ViewControllers.CorridorsNC)
    }
}

//MARK: - Functions
extension CorridorsViewController {
    private func setupUI() {
        //Table view
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        //Empty state
        self.emptyStateDataSource = self
        self.emptyStateDelegate = self
        
        self.showProgress("Consultando información")
        self.presenter.getCorridors()
    }
}

//MARK: - Empty State Delegate
extension CorridorsViewController: UIEmptyStateDataSource, UIEmptyStateDelegate {
    var emptyStateImage: UIImage? {
        return #imageLiteral(resourceName: "ic_empty")
    }
    
    var emptyStateImageSize: CGSize? {
        return CGSize(width: 180, height: 180)
    }
    
    var emptyStateTitle: NSAttributedString {
        let attrs : [NSAttributedStringKey : Any]  = [
            NSAttributedStringKey.foregroundColor: UIColor.black
        ]
        
        return NSAttributedString(string: "No se encontraron pasillos", attributes: attrs)
    }
    
    var emptyStateButtonTitle: NSAttributedString? {
        return nil
    }
}

//MARK: - Tableview Datasource & delegate
extension CorridorsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.corridors?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cells.Corridor, for: indexPath) as! CorridorTableViewCell
        cell.corridor = self.presenter.corridors![indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 220
    }
}

//MARK: - Corridors View
extension CorridorsViewController: CorridorsView {
    func didShowCorridors() {
        self.hideProgress() { _ in
            self.tableView.reloadData()
            self.reloadEmptyStateForTableView(self.tableView)
        }
    }
}
