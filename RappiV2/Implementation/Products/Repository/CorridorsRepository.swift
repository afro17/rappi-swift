//
//  CorridorsRepository.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/20/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class CorridorsRepository {
    static func getCorridorsFromServer(completion: @escaping(([Corridor]?) -> Void)) {
        let url = Constants.Api.URL + Constants.endpoints.corridors
        Alamofire.request(url, method: .get, parameters: nil, headers: Base.shared.userHeaders)
            .responseJSON {  response in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        let json = response.result.value as! [String: AnyObject]
                        let items: [Corridor] = Mapper<Corridor>().mapArray(JSONArray: json["info"] as! [[String : Any]])
                        completion(items)
                    } else {
                        completion(nil)
                    }
                } else {
                    completion(nil)
                }
        }
    }
}
