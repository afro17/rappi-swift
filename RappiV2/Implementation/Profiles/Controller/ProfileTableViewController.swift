//
//  ProfileTableViewController.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class ProfileTableViewController: UITableViewController {
    //MARK: - Properties
    let presenter = ProfilePresenter()
    
    //MARK: - IBOutlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var restartGameButton: TransitionButton!
    
    //MARK: - IBActions
    @IBAction func doLogOut(_ sender: UIButton) {
        self.presenter.logOut()
    }
    @IBAction func restartGame(_ sender: UIButton) {
        restartGameButton.backgroundColor = #colorLiteral(red: 0.1450980392, green: 0.768627451, blue: 0.5490196078, alpha: 1)
        restartGameButton.startAnimation()  
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.restartGameButton.stopAnimation(animationStyle: .expand, completion: {
                let likes = LikesViewController.storyboardInstance()
                self.present(likes, animated: true)
            })
        }
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter.attachView(self)
        
        self.setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    static func storyboardInstance() -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.Storyboards.Profile, bundle:nil)
        return storyBoard.instantiateViewController(withIdentifier: Constants.ViewControllers.Profile)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.1
        }
        return super.tableView(tableView, heightForHeaderInSection: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}

//MARK: - Functions
extension ProfileTableViewController {
    private func setupUI() {
        avatarImageView.layer.cornerRadius = 75
        avatarImageView.layer.masksToBounds = true
        avatarImageView.layer.borderColor = UIColor.white.cgColor
        avatarImageView.layer.borderWidth = 1
        
        self.showInfo()
    }
    
    private func showInfo() {
        if let user = Auth.auth().currentUser {
            self.nameLabel.text = user.displayName
            self.emailLabel.text = user.email
            if let url = user.photoURL {
                self.avatarImageView.sd_setImage(with: url)
            }
        }
    }
}

extension ProfileTableViewController: ProfileView {
    func didLogOut() {
        let destination = WelcomeViewController.storyboardInstance()
        self.present(destination, animated: true)
    }
}
