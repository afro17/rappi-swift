//
//  ProfilePresenter.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import Firebase
import FBSDKLoginKit

protocol ProfileView {
    func didLogOut()
}

class ProfilePresenter {
    private var view: ProfileView?
    
    func attachView(_ view: ProfileView) {
        self.view = view
    }
    
    func logOut() {
        FBSDKLoginManager().logOut()
        PreferencesManager.removeObject(key: Constants.Preferences.canPlay)
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            self.view?.didLogOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
}
