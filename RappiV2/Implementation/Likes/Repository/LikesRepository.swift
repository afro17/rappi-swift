//
//  LikesRepository.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class LikesRepository {
    static func getProductsFromServer(completion: @escaping(([Product]?) -> Void)) {
        let url = Constants.Api.URL + Constants.endpoints.products
        Alamofire.request(url, method: .get, parameters: nil, headers: Base.shared.userHeaders)
            .responseJSON {  response in
                if response.result.isSuccess {
                    if response.response?.statusCode == 200 {
                        let json = response.result.value as! [String: AnyObject]
                        let items: [Product] = Mapper<Product>().mapArray(JSONArray: json["info"] as! [[String : Any]])
                        completion(items)
                    } else {
                        completion(nil)
                    }
                } else {
                    completion(nil)
                }
        }
    }
    
    static func saveItemPreference(params: [String: Any], completion: @escaping((Bool) -> Void)) {
        let url = Constants.Api.URL + Constants.endpoints.likes
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: Base.shared.userHeaders)
            .responseJSON {  response in
                if response.result.isSuccess {
                    if response.response?.statusCode == 201 {
                       completion(true)
                    } else {
                        completion(false)
                    }
                } else {
                    completion(false)
                }
        }
    }
}
