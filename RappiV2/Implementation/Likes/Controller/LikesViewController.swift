//
//  LikesViewController.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import UIKit

class LikesViewController: UIViewController {
    
    //MARK: - Properties
    private let presenter = LikesPresenter()
    
    //MARK: - IBOutlets
    @IBOutlet private weak var swipeableCardView: SwipeableCardViewContainer!
    @IBOutlet weak var dislikeButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var buttonsStackView: UIStackView!
    @IBOutlet weak var restartStackView: UIStackView!
    @IBOutlet weak var finishButton: UIButton!
    
    //MARK: - IBActions
    @IBAction func finishGame(_ sender: UIButton) {
        PreferencesManager.setBool(key: Constants.Preferences.canPlay, value: true)
        let destination = MainViewController.storyboardInstance()
        self.present(destination, animated: true)
    }
    
    @IBAction func dislikeItem(_ sender: UIButton) {
        self.tapAnimation(view: dislikeButton.imageView!)
        self.swipeableCardView.removeCard(direction: .left)
    }
    @IBAction func likeItem(_ sender: UIButton) {
        self.tapAnimation(view: likeButton.imageView!)
        self.swipeableCardView.removeCard(direction: .right)
    }
    
    @IBAction func restartGame(_ sender: UIButton) {
        self.restartStackView.isHidden = true
        self.getProducts()
    }
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swipeableCardView.dataSource = self
        swipeableCardView.delegate = self
        
        self.presenter.attachView(self)
        
        self.getProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    static func storyboardInstance() -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.Storyboards.Likes, bundle:nil)
        return storyBoard.instantiateViewController(withIdentifier: Constants.ViewControllers.LikesVC)
    }
}

//MARK: - Functions
extension LikesViewController {
    private func getProducts() {
        self.showProgress("Consultando Información")
        self.presenter.getProducts()
    }
}

// MARK: - SwipeableCardViewDataSource
extension LikesViewController: SwipeableCardViewDataSource, SwipeableCardViewDelegate {
    func didHideSwipeableView() {
        self.swipeableCardView.isHidden = true
        self.buttonsStackView.isHidden = true
        self.restartStackView.isHidden = false
        self.finishButton.isHidden = true
    }
    
    func numberOfCards() -> Int {
        return self.presenter.items?.count ?? 0
    }
    
    func card(forItemAtIndex index: Int) -> SwipeableCardViewCard {
        let item = self.presenter.items![index]
        let cardView = SwipeableCardView()
        cardView.viewModel = item
        return cardView
    }
    
    func viewForEmptyCards() -> UIView? {
        return nil
    }
    
    func didSelect(card: SwipeableCardViewCard, at index: Int) {
        
    }
    
    func didSwipeCard(at index: Int, direction: SwipeDirection) {
        print("\(direction.stringValue) swipe card at index \(index)")
        if direction.horizontalPosition == .right {
            self.tapAnimation(view: likeButton.imageView!)
            self.presenter.save(like: true, at: index)
        } else {
            self.tapAnimation(view: dislikeButton.imageView!)
            self.presenter.save(like: false, at: index)
        }
    }
}

//MARK: - LikesView delegate
extension LikesViewController: LikesView {
    func didShowItems() {
        self.hideProgress() { _ in
            self.swipeableCardView.isHidden = false
            self.restartStackView.isHidden = true
            self.swipeableCardView.reloadData()
        }
    }
    
    func didShowButtons() {
        self.buttonsStackView.isHidden = false
        self.finishButton.isHidden = false
    }
}

//MARK: - Functions
extension LikesViewController {
    private func tapAnimation(view: UIImageView) {
        view.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.7,
                       delay: 0.2,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 10,
                       options: .curveLinear,
                       animations: {
                        view.transform = CGAffineTransform.identity
        })
    }
}

extension LikesViewController: UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadeTransition(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadeTransition(transitionDuration: 0.5, startingAlpha: 0.8)
    }
}
