//
//  IntroViewController.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/20/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var startButton: TransitionButton!
    
    //MARK: - IBActions
    @IBAction func playAction(_ sender: UIButton) {
        startButton.startAnimation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.startButton.stopAnimation(animationStyle: .expand, completion: {
                let likes = LikesViewController.storyboardInstance()
                self.present(likes, animated: true)
            })
        }
    }
    @IBAction func skipAction(_ sender: UIButton) {
        PreferencesManager.setBool(key: Constants.Preferences.canPlay, value: true)
        let destination = MainViewController.storyboardInstance()
        self.present(destination, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    static func storyboardInstance() -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.Storyboards.Likes, bundle:nil)
        return storyBoard.instantiateViewController(withIdentifier: Constants.ViewControllers.Intro)
    }
}
