//
//  LikesPresenter.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import UIKit

protocol LikesView {
    func didShowItems()
    func didShowButtons()
}

class LikesPresenter {
    //MARK: - Properties
    var items: [Product]?
    var view: LikesView?
    
    func attachView(_ view: LikesView) {
        self.view = view
    }
    
    func getProducts() {
        LikesRepository.getProductsFromServer() { items in
            self.items = items
            DispatchQueue.main.async {
                self.view?.didShowItems()
                if items?.count ?? 0 > 0 {
                    self.view?.didShowButtons()
                }
            }
        }
    }
    
    func save(like: Bool, at index: Int) {
        let item = self.items![index]
        let corridor = item.corridors?.first(where: {$0.parenId == nil})
        let params: [String: Any] = [
            "like": like,
            "product_id": item.id!,
            "corridor_id": corridor?.id,
            "corridor_name": corridor?.name
        ]
        
        LikesRepository.saveItemPreference(params: params) { _ in}
    }
}
