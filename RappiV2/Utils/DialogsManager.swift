//
//  DialogsManager.swift
//  Timesheets
//
//  Created by Andres Rojas on 7/4/18.
//  Copyright © 2018 Veryfi, Inc. All rights reserved.
//

import UIKit
import PKHUD

extension UIViewController {
    ///Displays a simple alert with no actions
    func alertMessage(title: String?, message: String) {
        DispatchQueue.main.async {
            let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            refreshAlert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { (action: UIAlertAction!) in
            }))
            self.present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    ///Displays a simple alert with no actions
    func alertMessage(title: String?, message: String, handler: @escaping((_ action: UIAlertAction) -> Void)) {
        DispatchQueue.main.async {
            let refreshAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            refreshAlert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: handler))
            self.present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    ///Displays a decision Alert
    func decisionAlert(title: String?, message: String, positiveTitle: String, positiveHandler: @escaping ((_ action: UIAlertAction) -> Void), negativeTitle: String, negativeHandler: ((_ action: UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: negativeTitle, style: .default, handler: negativeHandler))
            alert.addAction(UIAlertAction(title: positiveTitle, style: .default, handler: positiveHandler))
            self.present(alert, animated: true)
        }
    }
    
    ///Displays a decision Alert + Desctructive
    func decisionAlertDestructive(title: String?, message: String, positiveTitle: String, positiveHandler: @escaping ((_ action: UIAlertAction) -> Void), negativeTitle: String,  negativeHandler: ((_ action: UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: negativeTitle, style: .default, handler: negativeHandler))
            alert.addAction(UIAlertAction(title: positiveTitle, style: .destructive, handler: positiveHandler))
            self.present(alert, animated: true)
        }
    }
    
    ///Displays a decision ActionSheet
    func decisionActionSheet(
        title: String?,
        message: String,
        positiveTitle: String,
        positiveHandler: @escaping ((_ action: UIAlertAction) -> Void),
        negativeTitle: String,
        negativeHandler: ((_ action: UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: negativeTitle, style: .default, handler: negativeHandler))
            alert.addAction(UIAlertAction(title: positiveTitle, style: .default, handler: positiveHandler))
            if let presenter = alert.popoverPresentationController {
                presenter.sourceView = self.view
                presenter.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                presenter.permittedArrowDirections = []
            }
            self.present(alert, animated: true)
        }
    }
    
    ///Displays a decision ActionSheet
    func decisionActionSheetCancel(
        title: String?,
        message: String,
        positiveTitle: String,
        positiveHandler: @escaping ((_ action: UIAlertAction) -> Void),
        negativeTitle: String,
        negativeHandler: ((_ action: UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: negativeTitle, style: .cancel, handler: negativeHandler))
            alert.addAction(UIAlertAction(title: positiveTitle, style: .default, handler: positiveHandler))
            if let presenter = alert.popoverPresentationController {
                presenter.sourceView = self.view
                presenter.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                presenter.permittedArrowDirections = []
            }
            self.present(alert, animated: true)
        }
    }
    
    ///Displays a decision ActionSheet
    func decisionActionSheetDestructive(title: String?, message: String, positiveTitle: String, positiveHandler: @escaping ((_ action: UIAlertAction) -> Void), negativeTitle: String, negativeHandler: ((_ action: UIAlertAction) -> Void)? = nil) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: negativeTitle, style: .cancel, handler: negativeHandler))
            alert.addAction(UIAlertAction(title: positiveTitle, style: .destructive, handler: positiveHandler))
            if let presenter = alert.popoverPresentationController {
                presenter.sourceView = self.view
                presenter.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                presenter.permittedArrowDirections = []
            }
            self.present(alert, animated: true)
        }
    }
    
    
    func showProgress(_ text: String) {
        HUD.show(.labeledProgress(title: nil, subtitle: text))
    }
    
    func hideProgress(completion: ((Bool) -> Void)? = nil) {
        HUD.hide(animated: true, completion: completion)
    }
}
