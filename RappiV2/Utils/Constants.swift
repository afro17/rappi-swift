//
//  Constants.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Api {
        static let URL = "http://tuciudadenred.app:8084/api/v1/"
    }
    
    struct endpoints {
        static let products = "products/"
        static let likes = "userlikes/"
        static let users = "users/"
        static let corridors = "corridors/"
    }
    
    struct ViewControllers {
        static let WelcomeVC = "WelcomeViewController"
        static let WelcomeNC = "WelcomeNavController"
        static let LikesNC = "LikesNavController"
        static let LikesVC = "LikesViewController"
        static let Main = "MainViewController"
        static let Profile = "ProfileTableViewController"
        static let Intro = "IntroViewController"
        static let CorridorsVC = "CorridorsViewController"
        static let CorridorsNC = "CorridorsNavController"
    }
    
    struct Storyboards {
        static let Account = "Account"
        static let Likes = "Likes"
        static let Main = "Main"
        static let Profile = "Profile"
        static let Products = "Products"
        static let Corridors = "Corridors"
    }
    
    struct Preferences {
        static let IsLoggedInUser = "IsLoggedInUser"
        static let canPlay = "CanPlay"
    }
    
    struct Cells {
        static let Product = "ProductCell"
        static let Corridor = "CorridorCell"
    }
}
