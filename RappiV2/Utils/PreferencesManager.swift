//
//  PreferencesManager.swift
//  Timesheets
//
//  Created by Andres Rojas on 7/3/18.
//  Copyright © 2018 Veryfi, Inc. All rights reserved.
//

import Foundation


class PreferencesManager {
    
    static let userDefauls = UserDefaults.standard
    
    static func getString(key: String) -> String {
        if let string = userDefauls.string(forKey: key) {
            return string
        } else{
            return ""
        }
    }
    
    static func getBool(key: String) -> Bool {
        return userDefauls.bool(forKey: key)
    }
    
    static func getData(key: String) -> Data? {
        return userDefauls.object(forKey: key) as? Data
    }
    
    static func getDouble(key: String) -> Double {
        return userDefauls.double(forKey: key)
    }
    
    static func setBool(key: String, value: Bool) {
        userDefauls.set(value, forKey: key)
        userDefauls.synchronize()
    }
    
    static func setDouble(key: String, value: Double) {
        userDefauls.set(value, forKey: key)
        userDefauls.synchronize()
    }
    
    static func setString(key: String, value: String) {
        userDefauls.set(value, forKey: key)
        userDefauls.synchronize()
    }
    
    static func setInt(key: String, value: Int) {
        userDefauls.set(value, forKey: key)
        userDefauls.synchronize()
    }
    
    static func getInt(key: String) -> Int {
        return userDefauls.integer(forKey: key)
    }
    
    static func saveObject(data: Data, key: String) {
        userDefauls.set(data, forKey: key)
        userDefauls.synchronize()
    }
    
    static func removeObject(key: String) {
        userDefauls.removeObject(forKey: key)
        userDefauls.synchronize()
    }
}
