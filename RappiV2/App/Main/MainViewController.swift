//
//  MainViewController.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {
    
    //MARk: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createItems()
    }
    
    static func storyboardInstance() -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: Constants.Storyboards.Main, bundle:nil)
        return storyBoard.instantiateViewController(withIdentifier: Constants.ViewControllers.Main)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

//MARK: - Functions
extension MainViewController {
    private func createItems() {
        var items = [UIViewController]()
        let likes = CorridorsViewController.storyboardInstance()
        likes.tabBarItem = UITabBarItem(title: "Passillos", image: #imageLiteral(resourceName: "ic_cart"), tag: 0)
        items.append(likes)
        
        let profile = ProfileTableViewController.storyboardInstance()
        profile.tabBarItem = UITabBarItem(title: "Perfil", image: #imageLiteral(resourceName: "ic_profile"), tag: 0)
        items.append(profile)
        
        self.viewControllers = items
    }
}
