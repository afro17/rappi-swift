//
//  Base.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import Firebase

class Base {
    static let shared = Base()
    lazy var userHeaders: [String: String] = { [unowned self] in
        var headers = Base.basicHeaders
        headers["token"] = Auth.auth().currentUser!.uid
        return headers
    }()
    
    static var basicHeaders: [String: String] {
        return [
            "Accept": "application/json"
        ]
    }
}
