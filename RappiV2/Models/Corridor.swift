//
//  Corridor.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import ObjectMapper

class Corridor: Mappable {
    var id: Int!
    var name: String?
    var parenId: Int?
    var products: [Product]?
    
    required init?(map: Map) {}
    init() {}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        parenId <- map["parent_id"]
        products <- map["products"]
    }
}
