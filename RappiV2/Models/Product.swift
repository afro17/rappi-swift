//
//  Item.swift
//  RappiV2
//
//  Created by Andres Rojas on 10/19/18.
//  Copyright © 2018 Rappi V2. All rights reserved.
//

import Foundation
import ObjectMapper

class Product: Mappable {
    var id: Int!
    var name: String?
    var image: String?
    var price: Float?
    var corridors: [Corridor]?
    
    required init?(map: Map) {}
    init() {}
    
    func mapping(map: Map) {
        id <- map["product_id"]
        name <- map["name"]
        image <- map["image"]
        corridors <- map["corridors"]
        price <- map["price"]
    }
}
